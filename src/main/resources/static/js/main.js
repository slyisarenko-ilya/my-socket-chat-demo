 "use strict";
 
 var inpUsername = document.querySelector("#input-user-name");
 var formUsername = document.querySelector("#username-form");
 var lblUserName = document.querySelector("#display-user-name");
 var inpMessage = document.querySelector("#input-message");
 var formMessage = document.querySelector("#message-form");
 var pageMessage = document.querySelector("#chat-page");
 var btnSendUserName = document.querySelector("#btn-input-user-name");
 var btnSendMessage= document.querySelector("#btn-input-message");
 var messageArea = document.querySelector("#message-history");
 
 var pageUser= document.querySelector("#user-page");
 var pageChat= document.querySelector("#chat-page");
 
 var connectingElement = document.querySelector("#connecting-element");
 
 var stompClient = null;
 var username = null;
 
 function sendUserName(event){
	 console.log("sendUserName");

	 username = inpUsername.value.trim();
	 if(username){
		 
		 var socket = new SockJS('/ws');
		 stompClient = Stomp.over(socket);
		 stompClient.connect({}, onConnected, onError);
		 
	 }
	 event.preventDefault();
 }

 
 function sendMessage(event){
	 var messageContent = inpMessage.value.trim();
     if(messageContent && stompClient) {
    	 var chatMessage = {
    			 "sender": username,
    			 "type": "CHAT",
    			 "content": inpMessage.value
    	 };
    	 console.log("sendMessage: " + username + ", " + inpMessage.value);
    	 stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
    	 inpMessage.value = '';
     }
	 event.preventDefault();
 }
 
 
 function onConnected(){
	 console.log("onConnected");
	 
	 stompClient.subscribe("/topic/public", onMessageReceived);
	 
	 stompClient.send("/app/chat.addUser", {}, JSON.stringify({"sender": username, "type": "JOIN"}));
	 
     connectingElement.classList.add('hidden');
     
     lblUserName.innerText = username;
     
	 pageUser.classList.add("hidden");
	 pageMessage.classList.remove("hidden");

 }
 
 function onMessageReceived(payload){
	 console.log("onMessageReceived " + payload);
	 
	 var message = JSON.parse(payload.body);
     var messageElement = document.createElement('div');
     if(message.type === 'JOIN'){
    	 message.content = message.sender + " joined!";
     } else if(message.type === 'LEAVE'){
    	 message.content = message.sender + " left!";
     } else {
    	 var avatarElement = document.createElement('i');
         var avatarText = document.createTextNode(message.sender[0]);
         avatarElement.appendChild(avatarText);

         messageElement.appendChild(avatarElement);

         var usernameElement = document.createElement('span');
         var usernameText = document.createTextNode(message.sender);
         usernameElement.appendChild(usernameText);
         messageElement.appendChild(usernameElement);
     }
     
     var textElement = document.createElement('p');
     var messageText = document.createTextNode(message.content);
     textElement.appendChild(messageText);

     messageElement.appendChild(textElement);

     messageArea.appendChild(messageElement);
     messageArea.scrollTop = messageArea.scrollHeight;
 }
 
 
 function onError(error){
	 var errorMessage = 'Could not connect to WebSocket server. Please refresh this page to try again!';
	 	console.log("onError " + errorMessage);	
	    connectingElement.textContent = errorMessage;
	    connectingElement.style.color = 'red';
 }


 //define handlers for input elements: #input-user-name and #input-message
 function registerHandlers(){
	 formUsername.addEventListener("submit", sendUserName);
	 formMessage.addEventListener("submit", sendMessage);
 }
 
 //execute
 registerHandlers();
