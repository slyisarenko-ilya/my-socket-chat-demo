package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySocketChatDemoApplication {
    private static final Logger logger = LoggerFactory.getLogger(MySocketChatDemoApplication.class);

	public static void main(String[] args) {
		logger.info("start app");
		SpringApplication.run(MySocketChatDemoApplication.class, args);
		logger.info("after start app");

	}
}
